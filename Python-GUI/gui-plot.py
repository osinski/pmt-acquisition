from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import pyqtSlot, QTimer, Qt
from pyqtgraph import PlotWidget
import pyqtgraph as pg
import sys

from serialHandler import SerialHandler
from sdHandler import SDHandler
import numpy as np
import serial
import struct
import time
import math

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self._circularBufferSize = 512
        self._statisticsSize = 4
        self._adcResolution = 12
        self._fullScaleVoltage = 3300

        self._xAxisRange = [0, self._circularBufferSize]
        self._xAxisScalingFactor = 1

        self._yAxisRange = [0, 2**self._adcResolution]
        self._yAxisScalingFactor = 1

        self.ch1Delay = 0
        self.ch2Delay = 0
        self.threshold = 0

        self.serialConnection = SerialHandler(self._circularBufferSize,
                                              self._statisticsSize,
                                              math.ceil(self._adcResolution / 8))
        self.sdCard = SDHandler(self._circularBufferSize)

        uic.loadUi('mainwindow.ui', self)

        """ Window Title """
        self.setWindowTitle("OSG PMT Viewer")
        
        """ Status bar text """
        statusBarLabel_App = QtWidgets.QLabel(
            "Cyfrowe Kontrolery Sygnałów - monitorowanie sygnałów z PMT"
        )
        statusBarLabel_App.setAlignment(Qt.AlignLeft)

        statusBarLabel_Separator = QtWidgets.QLabel("    |    ")
        statusBarLabel_Separator.setAlignment(Qt.AlignCenter)

        statusBarLabel_Authors = QtWidgets.QLabel(
            "Marcin Osiński, Dominik Głuchowski, Dawid Szkaradek"
        )
        statusBarLabel_Authors.setAlignment(Qt.AlignRight)

        self.statusBar().addWidget(statusBarLabel_App)
        self.statusBar().addWidget(statusBarLabel_Separator)
        self.statusBar().addWidget(statusBarLabel_Authors)

        """ Fill Comboboxes """
        self.comboBox_samplingCtrl.addItems(['Triggered Only', 'Continuous'])
        self.comboBox_xAxis_usb.addItems(['Sample Number', 'Microseconds'])
        self.comboBox_yAxis_usb.addItems(['ADC Value', 'Milivolts'])
        self.comboBox_xAxis_sd.addItems(['Sample Number', 'Microseconds'])
        self.comboBox_yAxis_sd.addItems(['ADC Value', 'Milivolts'])

        """ Setup LCD """
        self.lcdNumber_ch1_delay.display(self._xAxisRange[1]//2)
        self.lcdNumber_ch2_delay.display(self._xAxisRange[1]//2)

        """ PyQtGraphs """
        self.Plot1.setRange(xRange=self._xAxisRange, yRange=self._yAxisRange)
        self.Plot2.setRange(xRange=self._xAxisRange, yRange=self._yAxisRange)
        infiniteLinePen = pg.mkPen("#C21807", width=2)
        self.pyqtgraphLine_ch1Thresh = pg.InfiniteLine(pos=0, angle=0,
                                                       pen=infiniteLinePen)
        self.Plot1.addItem(self.pyqtgraphLine_ch1Thresh)
        self.pyqtgraphLine_ch1Delay = pg.InfiniteLine(pos=0.5*self._xAxisRange[1],
                                                       pen=infiniteLinePen)
        self.Plot1.addItem(self.pyqtgraphLine_ch1Delay)
        self.pyqtgraphLine_ch2Thresh = pg.InfiniteLine(pos=0, angle=0,
                                                       pen=infiniteLinePen)
        self.Plot2.addItem(self.pyqtgraphLine_ch2Thresh)
        self.pyqtgraphLine_ch2Delay = pg.InfiniteLine(pos=0.5*self._xAxisRange[1],
                                                       pen=infiniteLinePen)
        self.Plot2.addItem(self.pyqtgraphLine_ch2Delay)
        self.Plot1.showGrid(x=True, y=True)
        self.Plot2.showGrid(x=True, y=True)
        self.Plot1_xAxis = self.Plot1.getAxis('bottom')
        self.Plot1_yAxis = self.Plot1.getAxis('left')
        self.Plot2_xAxis = self.Plot2.getAxis('bottom')
        self.Plot2_yAxis = self.Plot2.getAxis('left')
        self.majorTicksX = list(np.linspace(0, self._circularBufferSize, 5))
        self.minorTicksX = list(np.linspace(0, self._circularBufferSize, 9))
        self.minorTicksX = list(set(self.minorTicksX) - set(self.majorTicksX))
        self.majorTicksX = [int(tick) for tick in self.majorTicksX]
        self.minorTicksX = [int(tick) for tick in self.minorTicksX]
        self.Plot1_xAxis.setTicks([[(T, str(T)) for T in self.majorTicksX],
                                   [(t, str(t)) for t in self.minorTicksX]]) 
        self.Plot2_xAxis.setTicks([[(T, str(T)) for T in self.majorTicksX], 
                                   [(t, str(t)) for t in self.minorTicksX]]) 
        self.majorTicksY = list(np.linspace(0, 2**self._adcResolution - 1, 5))
        #minorTicks = list(np.linspace(0, 2**self._adcResolution, 9))
        #minorTicks = list(set(minorTicks) - set(majorTicks))
        self.majorTicksY = [round(tick, -3) for tick in self.majorTicksY[:-1]]
        self.majorTicksY.append(2**self._adcResolution - 1)
        self.majorTicksY = [int(tick) for tick in self.majorTicksY]
        #minorTicks = [int(tick) for tick in minorTicks]
        self.Plot1_yAxis.setTicks([[(T, str(T)) for T in self.majorTicksY]])
        self.Plot2_yAxis.setTicks([[(T, str(T)) for T in self.majorTicksY]]) 

        self.histogramAxisX = range(64)
        self.histogramCh1Values = [0] * len(self.histogramAxisX)
        self.histogramCh2Values = [0] * len(self.histogramAxisX)

        self.histogramCh1 = pg.BarGraphItem(x=self.histogramAxisX, 
                                            height=self.histogramCh1Values,
                                            width=0.5)
        self.histogramCh2 = pg.BarGraphItem(x=self.histogramAxisX,
                                            height=self.histogramCh2Values,
                                            width=0.5)

        self.Plot1_data = self.Plot1.plot([0]*self._xAxisRange[1])
        self.Plot2_data = self.Plot2.plot([0]*self._xAxisRange[1])
        self.Plot1_SDdata = self.Plot1.plot([0]*self._xAxisRange[1])
        self.Plot2_SDdata = self.Plot2.plot([0]*self._xAxisRange[1])

        """ Setup USB handling """
        self.comboBox_usbDevices.addItems(
            self.serialConnection.getConnectableDevices()
        )

        self.comboBox_sdCardSelection.addItems(
            self.sdCard.getAvailableDisks()
        )


    def plotData(self):
        self.Plot1_data.setData(
                self.serialConnection.dataPlot1 * self._yAxisScalingFactor
        )
        self.lineEdit_currentAmplitudeCh1.setText(
                str(self.serialConnection.amplitude1)
        )
        self.lineEdit_currentRiseTimeCh1.setText(
                str(self.serialConnection.riseTime1)
        )
        self.lineEdit_ppmCh1.setText(
                str(self.serialConnection.pulsesPerMinute1)
        )

        self.Plot2_data.setData(
                self.serialConnection.dataPlot2 * self._yAxisScalingFactor
        )
        self.lineEdit_currentAmplitudeCh2.setText(
                str(self.serialConnection.amplitude2)
        )
        self.lineEdit_currentRiseTimeCh2.setText(
                str(self.serialConnection.riseTime2)
        )
        self.lineEdit_ppmCh2.setText(
                str(self.serialConnection.pulsesPerMinute2)
        )

        """ Histograms """
        if self.serialConnection.histogramBarNumber1 != -1: 
            self.histogramCh1Values[self.serialConnection.histogramBarNumber1] += 1
            self.serialConnection.histogramBarNumber1 = -1
            self.histogramCh1.setOpts(height = self.histogramCh1Values)

        if self.serialConnection.histogramBarNumber2 != -1: 
            self.histogramCh1Values[self.serialConnection.histogramBarNumber2] += 1
            self.serialConnection.histogramBarNumber2 = -1
            self.histogramCh2.setOpts(height = self.histogramCh2Values)

        if self.tabWidget.currentIndex() == 2:
            self.Plot1.setRange(yRange=[0, max(self.histogramCh1Values) + 5])
            self.Plot2.setRange(yRange=[0, max(self.histogramCh2Values) + 5])


    @pyqtSlot(name='on_pushButton_usbCdcConnect_clicked')
    def connectToDevice(self):
        self.serialConnection.openSerialConnection(
            self.comboBox_usbDevices.currentText()
        )

        self.serialConnection.startBackgroundRecv()

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.plotData)
        self.timer.start(100)

    @pyqtSlot(name='on_pushButton_applySettings_clicked')
    def applySettings(self):
        samplingMode = self.comboBox_samplingCtrl.currentIndex()
        logEnabled = self.checkBox_enableLogging.isChecked()
        self.serialConnection.sendSettings(self.threshold, self.ch1Delay,
                                           self.ch2Delay, samplingMode,
                                           logEnabled)

    @pyqtSlot(name='on_pushButton_sdCardOpen_clicked')
    def setSDdirectory(self):
        self.sdCard.loadFiles(
            self.comboBox_sdCardSelection.currentIndex()
        )

        self.progressBar_wavCh1.setMaximum(self.sdCard.filesCountCh1)
        self.progressBar_wavCh2.setMaximum(self.sdCard.filesCountCh2)

    @pyqtSlot(name='on_pushButton_wavPrevCh1_clicked')
    def prevWaveformCh1(self):
        if self.sdCard.currentFileCh1 > 0:
            self.sdCard.currentFileCh1 -= 1

        self.progressBar_wavCh1.setValue(self.sdCard.currentFileCh1)

        self.sdCard.readFile(1)
        self.Plot1_SDdata.setData(
                self.sdCard.dataPlot1 * self._yAxisScalingFactor
        )

    @pyqtSlot(name='on_pushButton_wavNextCh1_clicked')
    def nextWaveformCh1(self):
        if self.sdCard.currentFileCh1 < self.sdCard.filesCountCh1:
            self.sdCard.currentFileCh1 += 1

        self.progressBar_wavCh1.setValue(self.sdCard.currentFileCh1)

        self.sdCard.readFile(1)
        self.Plot1_SDdata.setData(
                self.sdCard.dataPlot1 * self._yAxisScalingFactor
        )

    @pyqtSlot(name='on_pushButton_wavPrevCh2_clicked')
    def prevWaveformCh2(self):
        if self.sdCard.currentFileCh2 > 0:
            self.sdCard.currentFileCh2 -= 1

        self.progressBar_wavCh2.setValue(self.sdCard.currentFileCh2)

        self.sdCard.readFile(2)
        self.Plot2_SDdata.setData(
                self.sdCard.dataPlot1 * self._yAxisScalingFactor
        )

    @pyqtSlot(name='on_pushButton_wavNextCh2_clicked')
    def nextWaveformCh2(self):
        if self.sdCard.currentFileCh2 < self.sdCard.filesCountCh2:
            self.sdCard.currentFileCh2 += 1

        self.progressBar_wavCh2.setValue(self.sdCard.currentFileCh2)

        self.sdCard.readFile(2)
        self.Plot2_SDdata.setData(
                self.sdCard.dataPlot1 * self._yAxisScalingFactor
        )

    @pyqtSlot(int, name='on_dial_thresh_valueChanged')
    def dialThreshUpdated(self, newVal):
        setvalue = round(newVal/100 * self._yAxisRange[1])
        self.lcdNumber_thresh.display(setvalue)
        self.pyqtgraphLine_ch1Thresh.setValue(setvalue)
        self.pyqtgraphLine_ch2Thresh.setValue(setvalue)

        self.threshold = newVal

    @pyqtSlot(int, name='on_dial_ch1_delay_valueChanged')
    def dialChannel1DelayUpdated(self, newVal):
        setvalue = round(0.5*self._xAxisRange[1]+newVal/100*self._xAxisRange[1]//2)
        self.lcdNumber_ch1_delay.display(setvalue)
        self.pyqtgraphLine_ch1Delay.setValue(setvalue)

        self.ch1Delay = newVal

        areLinked = self.checkBox_linkChannelsReset.isChecked()
        isPressedDown = self.dial_ch1_delay.isSliderDown()
        if areLinked and isPressedDown:
            self.dial_ch2_delay.setValue(newVal)

    @pyqtSlot(int, name='on_dial_ch2_delay_valueChanged')
    def dialChannel2DelayUpdated(self, newVal):
        setvalue = round(0.5*self._xAxisRange[1]+newVal/100*self._xAxisRange[1]//2)
        self.lcdNumber_ch2_delay.display(setvalue)
        self.pyqtgraphLine_ch2Delay.setValue(setvalue)

        self.ch2Delay = newVal

        areLinked = self.checkBox_linkChannelsReset.isChecked()
        isPressedDown = self.dial_ch2_delay.isSliderDown()
        if areLinked and isPressedDown:
            self.dial_ch1_delay.setValue(newVal)

    @pyqtSlot(int, name='on_comboBox_xAxis_usb_currentIndexChanged')
    def xAxisDisplayControl(self, currentIndex):
        if currentIndex == 0:
            self._xAxisRange = [0, self._circularBufferSize]
            self.Plot1.setRange(xRange=self._xAxisRange)
            self.Plot2.setRange(xRange=self._xAxisRange)
        elif currentIndex == 1:
            self._xAxisRange = [0, self._circularBufferSize]
            self.Plot1.setRange(xRange=self._xAxisRange)
            self.Plot2.setRange(xRange=self._xAxisRange)

    @pyqtSlot(int, name='on_comboBox_yAxis_usb_currentIndexChanged')
    def yAxisDisplayControl(self, currentIndex):
        if currentIndex == 0:
            self._yAxisRange = [0, 2**self._adcResolution]
            self.Plot1.setRange(yRange=self._yAxisRange)
            self.Plot2.setRange(yRange=self._yAxisRange)
            self._yAxisScalingFactor = 1
        elif currentIndex == 1:
            self._yAxisRange = [0, self._fullScaleVoltage]
            self.Plot1.setRange(yRange=self._yAxisRange)
            self.Plot2.setRange(yRange=self._yAxisRange)
            self._yAxisScalingFactor = \
                                self._fullScaleVoltage / 2**self._adcResolution

    @pyqtSlot(int, name='on_tabWidget_currentChanged')
    def handleTabWidgetChange(self, index):
        if index == 0:
            #print('USB')
            self.Plot1.setRange(xRange=self._xAxisRange, yRange=self._yAxisRange)
            self.Plot2.setRange(xRange=self._xAxisRange, yRange=self._yAxisRange)
            self.Plot1_xAxis.setTicks([[(T, str(T)) for T in self.majorTicksX],
                                       [(t, str(t)) for t in self.minorTicksX]]) 
            self.Plot2_xAxis.setTicks([[(T, str(T)) for T in self.majorTicksX], 
                                       [(t, str(t)) for t in self.minorTicksX]]) 
            self.Plot1_yAxis.setTicks([[(T, str(T)) for T in self.majorTicksY]])
            self.Plot2_yAxis.setTicks([[(T, str(T)) for T in self.majorTicksY]]) 
            self.Plot1.showGrid(x=True, y=True)
            self.Plot2.showGrid(x=True, y=True)
            self.Plot1.addItem(self.pyqtgraphLine_ch1Thresh)
            self.Plot1.addItem(self.pyqtgraphLine_ch1Delay)
            self.Plot2.addItem(self.pyqtgraphLine_ch2Thresh)
            self.Plot2.addItem(self.pyqtgraphLine_ch2Delay)
            self.Plot1_data.setData([0]*self._xAxisRange[1])
            self.Plot2_data.setData([0]*self._xAxisRange[1])

            self.Plot1.removeItem(self.histogramCh1)
            self.Plot2.removeItem(self.histogramCh2)
            self.Plot1.removeItem(self.Plot1_SDdata)
            self.Plot2.removeItem(self.Plot2_SDdata)
            self.Plot1.addItem(self.Plot1_data)
            self.Plot2.addItem(self.Plot2_data)


        elif index == 1:
            #print('SD')
            self.Plot1.setRange(xRange=self._xAxisRange, yRange=self._yAxisRange)
            self.Plot2.setRange(xRange=self._xAxisRange, yRange=self._yAxisRange)
            self.Plot1_xAxis.setTicks([[(T, str(T)) for T in self.majorTicksX],
                                       [(t, str(t)) for t in self.minorTicksX]]) 
            self.Plot2_xAxis.setTicks([[(T, str(T)) for T in self.majorTicksX], 
                                       [(t, str(t)) for t in self.minorTicksX]]) 
            self.Plot1_yAxis.setTicks([[(T, str(T)) for T in self.majorTicksY]])
            self.Plot2_yAxis.setTicks([[(T, str(T)) for T in self.majorTicksY]]) 
            self.Plot1.showGrid(x=True, y=True)
            self.Plot2.showGrid(x=True, y=True)
            self.Plot1.removeItem(self.pyqtgraphLine_ch1Thresh)
            self.Plot1.removeItem(self.pyqtgraphLine_ch1Delay)
            self.Plot2.removeItem(self.pyqtgraphLine_ch2Thresh)
            self.Plot2.removeItem(self.pyqtgraphLine_ch2Delay)
            self.Plot1_data.setData([0]*self._xAxisRange[1])
            self.Plot2_data.setData([0]*self._xAxisRange[1])

            self.Plot1.removeItem(self.histogramCh1)
            self.Plot2.removeItem(self.histogramCh2)
            self.Plot1.removeItem(self.Plot1_data)
            self.Plot2.removeItem(self.Plot2_data)
            self.Plot1.addItem(self.Plot1_SDdata)
            self.Plot2.addItem(self.Plot2_SDdata)


        elif index == 2:
            #print('Statistics')
            self.Plot1.removeItem(self.Plot1_data)
            self.Plot2.removeItem(self.Plot2_data)
            self.Plot1.removeItem(self.Plot1_SDdata)
            self.Plot2.removeItem(self.Plot2_SDdata)
            self.Plot1.addItem(self.histogramCh1)
            self.Plot2.addItem(self.histogramCh2)
            self.Plot1.setRange(xRange=[-1,64], yRange=[0,10])
            self.Plot2.setRange(xRange=[-1,64], yRange=[0,10])
            self.Plot1.showGrid(x=False, y=False)
            self.Plot2.showGrid(x=False, y=False)
            self.Plot1_xAxis.setTicks(None)
            self.Plot1_yAxis.setTicks(None)
            self.Plot2_xAxis.setTicks(None)
            self.Plot2_yAxis.setTicks(None)



    
if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())
