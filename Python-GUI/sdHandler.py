import os
import psutil
import struct
from array import array

class SDHandler:
    def __init__(self, datalen):
        self.fileDataLen = datalen

        self.availableDisks = None

        self.currentFileCh1 = 0
        self.filesCountCh1 = 0
        self.currentFileCh2 = 0
        self.filesCountCh2 = 0

        self._fileListCh1 = list()
        self._fileListCh2 = list()

        self.dataPlot1 = list()
        self.dataPlot2 = list()

    def getAvailableDisks(self):
        partitions = psutil.disk_partitions()
        self.availableDisks = [(n.device, n.mountpoint) 
                                for n in partitions
                                if "fat" in str(n.fstype)
        ]

        return [devs[0] for devs in self.availableDisks]

    def loadFiles(self, selectedItem):
        mountPoint = self.availableDisks[selectedItem][1]
        for fileName in os.listdir(mountPoint):
            if os.path.isfile(os.path.join(mountPoint, fileName)):
                splitName = fileName.split('_')

                if splitName[2] == '0':
                    self._fileListCh1.append(os.path.join(mountPoint, fileName))
                    self.filesCountCh1 += 1

                elif splitName[2] == '1':
                    self._fileListCh2.append(os.path.join(mountPoint, fileName))
                    self.filesCountCh2 += 1


    def readFile(self, channel):
        data = array('H')

        if channel == 1:
            fileList = self._fileListCh1
            index = self.currentFileCh1
            plotData = self.dataPlot1
        
        elif channel == 2:
            fileList = self._fileListCh2
            index = self.currentFileCh2
            plotData = self.dataPlot2
        
        with open(fileList[index], 'rb') as f:
            data.fromfile(f, self.fileDataLen)
            sdData = list(struct.unpack('<{}H'.format(self.fileDataLen), data))
            plotData.clear()
            
            for item in sdData:
                plotData.append(item)
