/*
 * misc.h
 * Copyright (C) 2020 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef MISC_H
#define MISC_H

#include <utility>
#include <array>
#include <math.h>

#include "dac.h"

#include "peak_detector.h"


namespace PMT_Drive {
    using DriveDAC =  std::pair<DAC_HandleTypeDef*, uint8_t>;

    void setVoltage(DriveDAC dac, uint16_t newVoltage)
    {
        HAL_DAC_SetValue(dac.first, dac.second, DAC_ALIGN_12B_R, newVoltage);
    }
}

namespace Commander {
    struct Settings
    {
        uint8_t thresholdLevelPercentage = 0;
        uint8_t resetDelayCh1Percentage = 0;
        uint8_t resetDelayCh2Percentage = 0;
        bool continuousStream = false;
        bool loggingOnSD = false;
    };
}

#endif /* !MISC_H */
