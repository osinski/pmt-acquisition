/*
 * circular_buffer.h
 * Copyright (C) 2020 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

#include <array>
#include <utility>
#include <algorithm>
#include <stdint.h>

#include "dma.h"

enum PMT_buffer
{
    Buffer_1,
    Buffer_2
};

template <typename T = uint16_t, std::size_t size = 512>
class CircularBuffer
{
public:
    CircularBuffer(PMT_buffer bufferDesignation, ADC_TypeDef* hadc,
                    std::pair<DMA_TypeDef*, uint8_t> hdma) :
        bufferNumber { bufferDesignation },
        dmaHandler { hdma },
        adcHandler { hadc }
    {
        static_assert(((size & (size - 1)) == 0), "size has to be power of 2");
    }

    void start()
    {
        LL_DMA_SetMemoryAddress(dmaHandler.first, dmaHandler.second,
                                reinterpret_cast<uint32_t>(getDmaBufferAddr()));
        LL_DMA_SetPeriphAddress(dmaHandler.first, dmaHandler.second,
                LL_ADC_DMA_GetRegAddr(adcHandler, LL_ADC_DMA_REG_REGULAR_DATA));
        LL_DMA_SetDataLength(dmaHandler.first, dmaHandler.second, 
                             getDmaBufferSize());
        LL_DMA_EnableIT_TC(dmaHandler.first, dmaHandler.second);
        LL_DMA_EnableIT_HT(dmaHandler.first, dmaHandler.second);
        LL_DMA_EnableStream(dmaHandler.first, dmaHandler.second);

        LL_ADC_Enable(adcHandler);
        // no mention in rm to wait after setting ADON, but we do to be sure
        HAL_Delay(1000);

        LL_ADC_REG_StartConversionSWStart(adcHandler);
    }

    void triggerAction()
    {
        // check where DMA is
        dmaTriggerPos = LL_DMA_GetDataLength(dmaHandler.first, dmaHandler.second);

        if (dmaTriggerPos > (size / 2)) {
            // we're in the first part of the buffer and need to wait for 
            // transfer cplt interrupt

            waitForTransferComplete = true;
        }
        else {
            // we're in the second part of the buffer and need to wait for
            // half transfer cplt interrupt

            waitForHalfTransferComplete = true;
        }

        //// copy data written to DMA buf before trigger to first half of output
        //// buffer
        //if (elemsLeft <= size / 2) {
        //    auto firstElemIndex = size / 2 - elemsLeft;
        //    for (std::size_t i = 1; i < size / 2; i++) {
        //        bufOut.at(i) = bufDMA.at(firstElemIndex + i);
        //    }
        //} else {
        //    // how many elements are on the other side of circular buffer
        //    // size - elemsLeft (NDTR) = where are we from beginning of buffer
        //    // size / 2 - (size - elemsLeft) = how many elements are at the end
        //    // of buffer
        //    auto firstElemIndex = size - elemsLeft + size / 2;
        //    for (std::size_t j = 1; j < size / 2; j++) {
        //        bufOut.at(j) = bufDMA.at((firstElemIndex + j) & (size - 1));
        //    }
        //}
    }

    void transfferCpltAction()
    {
        std::copy_n(bufDMA + size*2 - dmaTriggerPos, size, begin(bufOut));

        
        waitForTransferComplete = false;
        readyToSend = true;
    }

    void halfTransferCpltAction()
    {
        std::copy(bufDMA + size*2 - dmaTriggerPos, bufDMA + size*2,
                  begin(bufOut));
        std::copy_n(bufDMA, size - dmaTriggerPos, 
                    begin(bufOut) + dmaTriggerPos);

        waitForHalfTransferComplete = false;
        readyToSend = true;

    }

    bool waitingForTransferComplete() const
    {
        return waitForTransferComplete;
    }

    bool waitingForHalfTransferComplete() const
    {
        return waitForHalfTransferComplete;
    }

    bool isReadyToSend() const
    {
        return readyToSend;
    }

    void bufSent()
    {
        readyToSend = false;
    }

    std::array<T, size>& getOutputBuffer()
    {
        return bufOut;
    }

    volatile T* getDmaBufferAddr()
    {
        return bufDMA;
    }

    std::size_t getDmaBufferSize() const
    {
        //return size * sizeof(T) / sizeof(uint32_t);
        return size * 2;
    }

    PMT_buffer getBufferDesignator() const
    {
        return bufferNumber;
    }

private:
    volatile T bufDMA[size*2]; 
    std::array<T, size> bufOut;

    std::pair<DMA_TypeDef*, uint8_t> dmaHandler;
    ADC_TypeDef* adcHandler;

    std::size_t dmaTriggerPos = 0;
    bool waitForTransferComplete = false;
    bool waitForHalfTransferComplete = false;
    bool readyToSend = false;

    PMT_buffer bufferNumber;

};


#endif /* !CIRCULAR_BUFFER_H */
