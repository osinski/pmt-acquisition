set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(MCPU_FLAGS "-mthumb -mcpu=cortex-m7")
set(VFP_FLAGS "-mfpu=fpv5-sp-d16 -mfloat-abi=hard")
set(LD_FLAGS "")

include(${CMAKE_CURRENT_LIST_DIR}/arm-none-eabi.cmake)
