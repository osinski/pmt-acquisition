#include <array>
#include <stdint.h>

#include "dma.h"
#include "spi.h"
#include "adc.h"
#include "usbd_cdc_if.h"
#include "fatfs.h"

#include "circular_buffer.h"
#include "peak_detector.h"
#include "usb_comm.h"
#include "sd_card.h"
#include "statistics.h"
#include "misc.h"

extern uint8_t UserRxBufferFS[APP_RX_DATA_SIZE];

bool receivedUSB;

CircularBuffer ADCbufferChannel1(PMT_buffer::Buffer_1, ADC1, 
                                {DMA2, LL_DMA_STREAM_0});
CircularBuffer ADCbufferChannel2(PMT_buffer::Buffer_2, ADC2, 
                                {DMA2, LL_DMA_STREAM_2});

PeakDetector peakDetector_ch1({TIM5, LL_TIM_CHANNEL_CH2});
PeakDetector peakDetector_ch2({TIM4, LL_TIM_CHANNEL_CH4});

Statistics statisticsChannel1;
Statistics statisticsChannel2;

SDCard sdCard;

extern "C" void app()
{

    Commander::Settings settings;

    receivedUSB = false;

    bool mountStatus = sdCard.mount();

    PeakDetector::setThreshold(33);

    LL_TIM_EnableCounter(TIM2);
    LL_TIM_EnableIT_UPDATE(TIM2);

    //HAL_ADC_Start_DMA(&hadc1, reinterpret_cast<uint32_t*>(
    //                            ADCbufferChannel1.getDmaBufferAddr()
    //                          ), ADCbufferChannel1.getSize());
    //HAL_ADC_Start_DMA(&hadc2, reinterpret_cast<uint32_t*>(
    //                            ADCbufferChannel2.getDmaBufferAddr()
    //                          ), ADCbufferChannel2.getSize());

    ADCbufferChannel1.start();
    //ADCbufferChannel2.start();


    while (true) {
        if (receivedUSB) {
            receivedUSB = false;

            USB::receiveSettings(&settings, UserRxBufferFS);
            
            PeakDetector::setThreshold(settings.thresholdLevelPercentage);
            peakDetector_ch1.setResetDelay(settings.resetDelayCh1Percentage);
            peakDetector_ch2.setResetDelay(settings.resetDelayCh2Percentage);
        }

        if (ADCbufferChannel1.isReadyToSend()) {
            statisticsChannel1.newPulse(ADCbufferChannel1.getOutputBuffer(),
                                        settings.resetDelayCh1Percentage);

            USB::sendData(ADCbufferChannel1.getOutputBuffer(),
                          ADCbufferChannel1.getBufferDesignator(),
                          statisticsChannel1.getAmplitude(),
                          statisticsChannel1.getRiseTime(),
                          statisticsChannel1.getPulsesPerMinute(),
                          statisticsChannel1.getHistogramBarToIncrement());

            ADCbufferChannel1.bufSent();

            if (settings.loggingOnSD && mountStatus) {
                sdCard.writeWaveform(ADCbufferChannel1.getOutputBuffer(),
                                     ADCbufferChannel1.getBufferDesignator());
            }
        }

        if (ADCbufferChannel2.isReadyToSend()) {
            statisticsChannel1.newPulse(ADCbufferChannel2.getOutputBuffer(),
                                        settings.resetDelayCh2Percentage);

            USB::sendData(ADCbufferChannel2.getOutputBuffer(),
                          ADCbufferChannel2.getBufferDesignator(),
                          statisticsChannel2.getAmplitude(),
                          statisticsChannel2.getRiseTime(),
                          statisticsChannel2.getPulsesPerMinute(),
                          statisticsChannel2.getHistogramBarToIncrement());

            ADCbufferChannel1.bufSent();

            if (settings.loggingOnSD && mountStatus) {
                sdCard.writeWaveform(ADCbufferChannel2.getOutputBuffer(),
                                     ADCbufferChannel2.getBufferDesignator());
            }
        }

        if (settings.continuousStream) {
            // TODO
            ADCbufferChannel1.triggerAction();
            //peakDetector_ch1.triggerReset();
            //peakDetector_ch2.triggerReset();
            HAL_Delay(100);
            ADCbufferChannel2.triggerAction();
            //peakDetector_ch2.triggerReset();
            HAL_Delay(100);
        }
    }
}

extern "C" void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    switch (GPIO_Pin) {
    case SIGNAL_FLAG_1_Pin:
        peakDetector_ch1.triggerReset();
        ADCbufferChannel1.triggerAction();        
        break;

    case SIGNAL_FLAG_2_Pin:
        peakDetector_ch2.triggerReset();
        ADCbufferChannel2.triggerAction();
        break;
    
    case BTN1_Pin:
        break;

    case BTN2_Pin:
        break;

    default:
        break;
    }
}

extern "C" void DMA2_Stream0_IRQHandler(void)
{
    if (ADCbufferChannel1.waitingForHalfTransferComplete() && 
        LL_DMA_IsActiveFlag_HT0(DMA2))
    {
        ADCbufferChannel1.halfTransferCpltAction();
    }
    else if (ADCbufferChannel1.waitingForTransferComplete() &&
        LL_DMA_IsActiveFlag_TC0(DMA2))
    {
        ADCbufferChannel1.transfferCpltAction();
    }
}

extern "C" void DMA2_Stream2_IRQHandler(void)
{
    if (ADCbufferChannel2.waitingForHalfTransferComplete() &&
        LL_DMA_IsActiveFlag_HT0(DMA2))
    {
        ADCbufferChannel2.halfTransferCpltAction();
    }
    else if (ADCbufferChannel2.waitingForTransferComplete() &&
        LL_DMA_IsActiveFlag_TC0(DMA2))
    {
        ADCbufferChannel2.transfferCpltAction();
    }
}

extern "C" void TIM2_IRQHandler(void)
{
    LL_TIM_ClearFlag_UPDATE(TIM2);

    statisticsChannel1.newTick();
    statisticsChannel2.newTick();
}


//extern "C" void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
//{
//    if (hadc == &hadc1 && ADCbufferChannel1.xferCpltActionEnabled()) {
//        ADCbufferChannel1.xferCpltAction();
//    }
//
//    if (hadc == &hadc2 && ADCbufferChannel2.xferCpltActionEnabled()) {
//        ADCbufferChannel2.xferCpltAction();
//    }
//}

