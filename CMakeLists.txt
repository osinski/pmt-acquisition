cmake_minimum_required(VERSION 3.13)
project(PMT_ACQUISITION
        LANGUAGES ASM C CXX
        VERSION 1.0
        DESCRIPTION "STM software for PMT acquisition card"
)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)
set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED True)

set(FILE_ELF ${CMAKE_PROJECT_NAME}.elf)
set(FILE_HEX ${CMAKE_PROJECT_NAME}.hex)

#####   OPTIONS     ############################################################
option(GENERATE_MAP "Generate *.map file" OFF)

option(FLASH_AFTER_BUILD "Flash MCU automatically after build" OFF)
option(DEBUG_AFTER_BUILD "Flash MCU and run GDB automatically after build" OFF)

option(FORCE_COLORED_OUTPUT "Always produce ANSI-colored output (GNU/Clang)." OFF)
################################################################################

if(GENERATE_MAP)
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS}
                                 -Wl,-Map=${CMAKE_PROJECT_NAME}.map
                                 -Wl,--cref"
    )
endif()

if (FORCE_COLORED_OUTPUT)
    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
       add_compile_options (-fdiagnostics-color=always)
    elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
       add_compile_options (-fcolor-diagnostics)
    endif ()
endif ()

# Add linker script to link options
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS}\
                                -T${PROJECT_SOURCE_DIR}/STM32F722RETx_FLASH.ld")

add_subdirectory(Drivers)

add_executable(${FILE_ELF}
    ${CMAKE_CURRENT_SOURCE_DIR}/startup_stm32f722xx.s
)

add_subdirectory(Core)
add_subdirectory(Middlewares)
add_subdirectory(FATFS)
add_subdirectory(USB_DEVICE)
add_subdirectory(App)


target_link_libraries(${FILE_ELF} hal)

target_compile_options(${FILE_ELF}
    PRIVATE
        $<$<COMPILE_LANGUAGE:NOT ASM>:" -Wall -Wextra">
)

#####   POST BUILD  ############################################################
add_custom_command(TARGET ${FILE_ELF} POST_BUILD
                    COMMENT "Creating .hex file.."
                    COMMAND ${OBJCOPY} -O ihex ${FILE_ELF} ${FILE_HEX}
)

add_custom_command(TARGET ${FILE_ELF} POST_BUILD
                    COMMENT "Calculating size.."
                    COMMAND ${OBJSIZE} ${FILE_ELF}
)



