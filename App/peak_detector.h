/*
 * peak_detector.h
 * Copyright (C) 2020 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef PEAK_DETECTOR_H
#define PEAK_DETECTOR_H

#include <utility>

#include "tim.h"
#include "dac.h"

extern DAC_HandleTypeDef hdac;

class PeakDetector
{
public:
    PeakDetector(std::pair<TIM_TypeDef*, uint32_t> timer) :
        detectorTimer { timer }
    {}

    static void setThreshold(uint16_t newThreshold)
    {
        auto detectorDAC_handler = hdac;
        const auto detectorDAC_channel = DAC_CHANNEL_2;

        constexpr auto fullScaleCode = 4095;
        newThreshold = newThreshold * fullScaleCode / 100;

        HAL_DAC_Stop(&detectorDAC_handler, detectorDAC_channel);

        HAL_DAC_SetValue(&detectorDAC_handler, detectorDAC_channel,
                            DAC_ALIGN_12B_R, newThreshold);
        HAL_DAC_Start(&detectorDAC_handler, detectorDAC_channel);
    }

    void setResetDelay(uint8_t newDelay)
    {
        resetDelay = newDelay;
    }

    void triggerReset()
    {
        auto resetDelayCounts = resetDelay * rstPulseWidth / 100;
        resetDelayCounts += 25;
        resetDelayCounts /= 2; // compensation value obtained experimentally

        if (detectorTimer.second == LL_TIM_CHANNEL_CH2) {
            LL_TIM_OC_SetCompareCH2(detectorTimer.first, resetDelayCounts);
        } else if (detectorTimer.second == LL_TIM_CHANNEL_CH4) {
            LL_TIM_OC_SetCompareCH4(detectorTimer.first, resetDelayCounts);
        } else {
            while (!!true);
        }
        
        LL_TIM_SetAutoReload(detectorTimer.first, resetDelayCounts + 
                                                  rstPulseWidth);
        
        LL_TIM_GenerateEvent_UPDATE(detectorTimer.first); 

        LL_TIM_EnableCounter(detectorTimer.first);

        //__HAL_TIM_SET_COMPARE(detectorTimer.first, detectorTimer.second,
        //                        resetDelayCounts);
        //__HAL_TIM_SET_AUTORELOAD(detectorTimer.first, resetDelayCounts +
        //                                              rstPulseWidth);
        //HAL_TIM_OnePulse_Start(detectorTimer.first, detectorTimer.second);

        //__HAL_TIM_ENABLE(detectorTimer.first);
    }

private:
    std::pair<TIM_TypeDef*, uint32_t> detectorTimer;
    std::pair<DAC_HandleTypeDef*, uint8_t> detectorDAC;
    //const uint8_t rstPulseWidth = 10;
    static constexpr uint8_t rstPulseWidth = 255;
    uint8_t resetDelay = 0;
};

#endif /* !PEAK_DETECTOR_H */
