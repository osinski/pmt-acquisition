/*
 * usb_comm.h
 * Copyright (C) 2020 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef USB_COMM_H
#define USB_COMM_H

#include <array>

#include "usbd_cdc_if.h"

#include "misc.h"
#include "circular_buffer.h"

namespace USB
{
    void receiveSettings(Commander::Settings* settings, uint8_t* rxBuf)
    {
        memcpy(&settings->thresholdLevelPercentage, rxBuf, 2);
        memcpy(&settings->resetDelayCh1Percentage, rxBuf+2, 1);
        memcpy(&settings->resetDelayCh2Percentage, rxBuf+3, 1);
        memcpy(&settings->continuousStream, rxBuf+4, 1);
        memcpy(&settings->loggingOnSD, rxBuf+5, 1);
    }

    // TODO histogramInfo can easily be a byte too
    template<class Buffer>
    void sendData(Buffer& txBuf, PMT_buffer bufferNumber, uint16_t amplitude,
                  uint8_t riseTime, uint16_t pulsesPerMinute, 
                  uint16_t histogramInfo)
    {
        static_assert(!std::is_array<Buffer>::value, 
                      "Buffer should be std::array"
        );

        static uint16_t statisticsBuf[4]; 
        statisticsBuf[0] = amplitude;
        statisticsBuf[1] = pulsesPerMinute;
        statisticsBuf[2] = histogramInfo;
        statisticsBuf[3] = static_cast<uint16_t>(riseTime);

        // TODO make it better and make txBuf const
        txBuf[0] = bufferNumber;
        while (CDC_Transmit_FS(reinterpret_cast<uint8_t*>(txBuf.data()), 
                    sizeof(txBuf)) == USBD_BUSY)
        {
            HAL_Delay(10);
        }

        while (CDC_Transmit_FS(reinterpret_cast<uint8_t*>(statisticsBuf), 
                    sizeof(statisticsBuf)) == USBD_BUSY)
        {
            HAL_Delay(10);
        }
    }

};

#endif /* !USB_COMM_H */
