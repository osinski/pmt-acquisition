/*
 * statistics.h
 * Copyright (C) 2020 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef STATISTICS_H
#define STATISTICS_H

#include <stdint.h>
#include <array>
#include <algorithm>
#include <functional>
#include <numeric>


template <typename T = uint16_t, std::size_t size = 512>
class Statistics
{
public:
    void newTick()
    {
        if (++systickCounter == 60) { // one minute passed
            // calculate pulses per seconds
            pulsesPerMinute = pulsesCount;
            pulsesCount = 0;
            systickCounter = 0;
        }
    }

    void newPulse(const std::array<T,size>& pulseToAnalyze, 
                  uint8_t resetDelayPercentage)
    {
        std::copy(begin(pulseToAnalyze), end(pulseToAnalyze), begin(pulse));

        pulsesCount++;

        calculateAmplitude(resetDelayPercentage * (size / 2) / 100);
        calculateRiseTime();
        determineHistogramBar();
    }

    uint16_t getPulsesPerMinute() const
    {
        return pulsesPerMinute;
    }

    uint8_t getRiseTime() const 
    {
        return riseTime;
    }

    uint16_t getAmplitude() const
    {
        return amplitude;
    }

    uint8_t getHistogramBarToIncrement() const
    {
        return histogramBarToIncrement;
    }

private:
    uint16_t systickCounter = 0;
    uint16_t pulsesCount = 0;
    uint16_t pulsesPerMinute = 0;

    uint8_t riseTime = 0;
    uint16_t amplitude = 0;
    uint16_t histogramBarToIncrement = 0;
    static constexpr uint8_t histogramBars = 64;

    std::array<T,size> pulse;
    
    uint16_t pulseHighBegin = 0;

    void calculateRiseTime()
    {
        uint16_t floor = findFloor();
        uint16_t ninetyPercentValue = 0.90f * (amplitude - floor) + floor;
        uint16_t tenPercentValue = 0.10f * (amplitude - floor) + floor;
        uint16_t ninetyPercentSample = 0;
        uint16_t tenPercentSample = 0;

        auto i = pulseHighBegin;


        //needed because we did sort on pulse
        while (pulse.at(i) < ninetyPercentValue) {
            i--;
            if (i > size || i <= 0) {
                i = pulseHighBegin;
                break;
            }
        }

        while (pulse.at(i) > ninetyPercentValue) {
            i--;
        }

        ninetyPercentSample = i;

        while (pulse.at(i) > tenPercentValue) {
            i--;
        }

        tenPercentSample = i;

        riseTime = ninetyPercentSample - tenPercentSample;
    }

    void calculateAmplitude(uint8_t approxResetPosition)
    {
        pulseHighBegin = findEdge(LEFT);
        // TODO verify if 20 is good enough
        auto pulseHighEnd = findEdge(RIGHT, size / 2 + approxResetPosition - 20);

        //std::vector<T> tempBuf;
        //tempBuf.resize(pulseHighEnd - pulseHighBegin);

        //std::copy(pulse[pulseHighBegin], pulse[pulseHighEnd], tempBuf);

        //std::sort(begin(tempBuf), end(tempBuf));
        
        // we sort so we can take a median to eliminate high frequency content
        std::sort(begin(pulse)+pulseHighBegin, begin(pulse)+pulseHighEnd);
        // now we return the median value
        amplitude = pulse.at(pulseHighBegin + (pulseHighEnd - pulseHighBegin) / 2);
    }

    void determineHistogramBar()
    {
        histogramBarToIncrement = amplitude / histogramBars;
    }

    enum Direction {
        LEFT,
        RIGHT
    };

    uint16_t findEdge(const Direction direction, 
                      const uint16_t startingPoint = size / 2,
                      const uint8_t checkRange = 3)
    {
        auto edgeIndex = startingPoint;
        bool edgeNotFound = true;

        if (direction == LEFT) {
            while (edgeNotFound) {
                // std::less makes it check in descending order
                if (std::is_sorted(begin(pulse) + edgeIndex - checkRange,
                                   begin(pulse) + edgeIndex,
                                   std::less<>()))
                {
                    edgeNotFound = false;
                }

                else {
                    edgeIndex--;
                }
            }
        } else {
            while (edgeNotFound) {
                if (std::is_sorted(begin(pulse) + edgeIndex,
                                   begin(pulse) + edgeIndex + checkRange,
                                   std::less<>()))
                {
                    edgeNotFound = false;
                }

                else {
                    edgeIndex++;
                }
            }
        }

        return edgeIndex;
    }

    uint16_t findFloor()
    {
        uint16_t first = (size / 2) * 0.25f;
        uint16_t last = (size / 2) * 0.75f;
        auto sum = std::accumulate(begin(pulse) + first,
                                   begin(pulse) + last,
                                   0);

        return sum / (last - first);
    }
};

#endif /* !STATISTICS_H */
