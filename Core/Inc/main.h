/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"
#include "stm32f7xx_ll_adc.h"
#include "stm32f7xx_ll_dma.h"
#include "stm32f7xx_ll_tim.h"
#include "stm32f7xx_ll_bus.h"
#include "stm32f7xx_ll_cortex.h"
#include "stm32f7xx_ll_rcc.h"
#include "stm32f7xx_ll_system.h"
#include "stm32f7xx_ll_utils.h"
#include "stm32f7xx_ll_pwr.h"
#include "stm32f7xx_ll_gpio.h"

#include "stm32f7xx_ll_exti.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SIGNAL_ADC_2_Pin GPIO_PIN_0
#define SIGNAL_ADC_2_GPIO_Port GPIOC
#define SIGNAL_FLAG_2_Pin GPIO_PIN_2
#define SIGNAL_FLAG_2_GPIO_Port GPIOC
#define LED_G_1_Pin GPIO_PIN_2
#define LED_G_1_GPIO_Port GPIOA
#define LED_Y_1_Pin GPIO_PIN_3
#define LED_Y_1_GPIO_Port GPIOA
#define PMT_DRIVE_DAC_Pin GPIO_PIN_4
#define PMT_DRIVE_DAC_GPIO_Port GPIOA
#define DAC_COMPARATOR_Pin GPIO_PIN_5
#define DAC_COMPARATOR_GPIO_Port GPIOA
#define SIGNAL_FLAG_1_Pin GPIO_PIN_6
#define SIGNAL_FLAG_1_GPIO_Port GPIOA
#define SIGNAL_FLAG_1_EXTI_IRQn EXTI9_5_IRQn
#define SIGNAL_ADC_1_Pin GPIO_PIN_0
#define SIGNAL_ADC_1_GPIO_Port GPIOB
#define LED3_Pin GPIO_PIN_1
#define LED3_GPIO_Port GPIOB
#define LED1_Pin GPIO_PIN_10
#define LED1_GPIO_Port GPIOB
#define LED2_Pin GPIO_PIN_11
#define LED2_GPIO_Port GPIOB
#define BUZZER_Pin GPIO_PIN_12
#define BUZZER_GPIO_Port GPIOB
#define BTN2_Pin GPIO_PIN_14
#define BTN2_GPIO_Port GPIOB
#define BTN2_EXTI_IRQn EXTI15_10_IRQn
#define BTN1_Pin GPIO_PIN_15
#define BTN1_GPIO_Port GPIOB
#define BTN1_EXTI_IRQn EXTI15_10_IRQn
#define SD_CS_Pin GPIO_PIN_15
#define SD_CS_GPIO_Port GPIOA
#define CARD_DETECT_Pin GPIO_PIN_12
#define CARD_DETECT_GPIO_Port GPIOC
#define LED_G_2_Pin GPIO_PIN_4
#define LED_G_2_GPIO_Port GPIOB
#define LED_Y_2_Pin GPIO_PIN_5
#define LED_Y_2_GPIO_Port GPIOB
#define SIGNAL_FLAG_2B7_Pin GPIO_PIN_7
#define SIGNAL_FLAG_2B7_GPIO_Port GPIOB
#define SIGNAL_FLAG_2B7_EXTI_IRQn EXTI9_5_IRQn
/* USER CODE BEGIN Private defines */
#define SD_SPI_HANDLE hspi3
#define SD_CS_GPIO_Port GPIOA
#define SD_CS_Pin GPIO_PIN_15
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
