import serial
from serial.tools import list_ports
import struct
import time
from threading import Thread

class SerialHandler:
    def __init__(self, RxBufSize, statisticsSize, bytesPerValue):
        self.datalen = RxBufSize + statisticsSize
        self.vallen = bytesPerValue
        self.serial = None
        self.rxBuf = bytearray(self.vallen * self.datalen)

        self.dataPlot1 = list()
        self.amplitude1 = 0
        self.riseTime1 = 0
        self.pulsesPerMinute1 = 0
        self.histogramBarNumber1 = 0

        self.dataPlot2 = list()
        self.amplitude2 = 0
        self.riseTime2 = 0
        self.pulsesPerMinute2 = 0
        self.histogramBarNumber2 = 0


    def getConnectableDevices(self):
        try:
            return list_ports.comports()[0]
        except:
            return ["No ports available"]

    def openSerialConnection(self, usbDevice):
        self.serial = serial.Serial(usbDevice)

    def startBackgroundRecv(self):
        self.thread = Thread(target=self.recvFunction)
        self.thread.start()
        time.sleep(0.5)
        self.serial.reset_input_buffer()

    def recvFunction(self):
        while(True):
            self.rxBuf = self.serial.read(self.datalen*self.vallen)
            self.rxBuf = list(struct.unpack('<{}H'.format(self.datalen),
                                        self.rxBuf))
            #print(self.rxBuf)
            #print('=======================================')
            if self.rxBuf[0] == 0:
                self.dataPlot1 = self.rxBuf[:-4]
                self.dataPlot1[0] = self.rxBuf[1]
                self.amplitude1 = self.rxBuf[-4]
                self.pulsesPerMinute1 = self.rxBuf[-3]
                self.histogramBarNumber1 = self.rxBuf[-2]
                self.riseTime1 = self.rxBuf[-1]
                #print(self.amplitude1)
                #print(self.pulsesPerMinute1)
                #print(self.histogramBarNumber1)
                #print(self.riseTime1)
                #print('==========================')
            elif self.rxBuf[0] == 1:
                self.dataPlot2 = self.rxBuf[:-4]
                self.dataPlot2[0] = self.rxBuf[1]
                self.amplitude2 = self.rxBuf[-4]
                self.pulsesPerMinute2 = self.rxBuf[-3]
                self.histogramBarNumber2 = self.rxBuf[-2]
                self.riseTime2 = self.rxBuf[-1]

    def sendSettings(self, threshold, ch1delay, ch2delay, samplingMode, logEnabled):
        txBuf1 = struct.pack('<1H', threshold)
        txBuf2 = struct.pack('<4B', ch1delay, ch2delay, samplingMode, logEnabled)
        txBuf = txBuf1 + txBuf2

        #print(threshold)
        #print(ch1delay)
        #print(ch2delay)
        #print(samplingMode)
        #print(logEnabled)
        #print(txBuf1)
        #print(txBuf2)
        #print(txBuf)

        self.serial.write(txBuf)

