/*
 * sd_card.h
 * Copyright (C) 2020 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef SD_CARD_H
#define SD_CARD_H

#include "stdio.h"

#include "fatfs.h"

#include "circular_buffer.h"


class SDCard
{
public:
    SDCard()
    {

    }

    [[nodiscard]]
    bool mount()
    {
        FRESULT result = f_mount(&fatfs, "", 1);

        if (result != FR_OK)
            return false;
        else
            return true;
    }

    template<class Buffer>
    bool writeWaveform(Buffer& waveform, PMT_buffer channel)
    {
        static uint16_t waveformCounterCh1 = 0, waveformCounterCh2 = 0;

        TCHAR filename[filenameLen];

        if (channel == Buffer_1) {
            sprintf(filename, "wav_%05u_%u", waveformCounterCh1++, 0);
        } else {
            sprintf(filename, "wav_%05u_%u", waveformCounterCh2++, 1);
        }


        if (f_open(&file, filename, FA_CREATE_NEW | FA_WRITE) != FR_OK)
            return false;

        UINT bw = 0; // bytes written
        auto status = f_write(&file, waveform.data(), sizeof(waveform), &bw);
        if (status != FR_OK && sizeof(waveform) != bw)
            return false;

        if (f_close(&file) != FR_OK)
            return false;

        return true;
    }

    //template<class Buffer>
    //[[nodiscard]] 
    //bool readWaveform(Buffer& waveform, std::pair<uint16_t, PMT_buffer> fileID)

    //{
    //    TCHAR filename[filenameLen];

    //    sprintf(filename, "wav_%05u_%u", fileID.first, fileID.second);

    //    FIL file;

    //    if (f_open(&file, filename, FA_OPEN_EXISTING | FA_READ) != FR_OK)
    //        return false;

    //    auto br = 0; // bytes read
    //    auto status = f_write(&file, waveform.data(), sizeof(waveform), &br);
    //    if (status != FR_OK && sizeof(waveform) != br)
    //        return false;

    //    if (f_close(&file) != FR_OK)
    //        return false;

    //    return true;
    //}

private:
    static constexpr auto filenameLen = 12;

    FATFS fatfs;
    FIL file;
};

#endif /* !SD_CARD_H */
